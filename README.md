# Arch Linux Autoinstall

Automatic installation scripts for Arch Linux.

## Description

This set of bash scripts (referred to as "the scripts") will do an **unattended** basic installation of Arch Linux (w/o desktop environment) on **x64 UEFI platform**. The installation can be customized by simply changing the settings in `install_settings.sh`. It strictly follows the official [Installation Guide](https://wiki.archlinux.org/title/Installation_guide). You have to continue the [Post-installation](https://wiki.archlinux.org/title/Installation_guide#Post-installation) manually, except for some tasks which can be done in chroot environment by `post_install` function in `install_settings.sh`. The scripts can also install Arch Linux for diskless workstation (which is called "net boot" in the comments) by set `boot_type` to `net` as well as other `nb_*` settings in `install_settings.sh`. 

**IMPORTANT NOTE**

To prevent possible data loss, the scripts only automatically create partitons on empty disks (i.e. w/o existing partitions) in default settings. Therefore you must confirm the deletion of partitions. However, if automatic deletion is desired, set both `auto_confirm` and `auto_confirm_partdel` to `true`. **Use it with caution**.

### Requirements

An Internet connection is required because the system clock will be synchronized to NTP server and some packages will be installed from Arch Linux mirrors.

### Limitations

- Supports only UEFI-x64 boot mode.
- Supports only GRUB2 bootloader for local boot, GRUB2 or PXELINUX for net boot.
- Simple disk partition scheme with LVM (cf. the `Partition sizes` section in `install_settings.sh`).
- Network manager supports only `NetworkManager` or `systemd-networkd`.
- Network configuration supports only wired adapters.

## Usage

1. Boot the Arch Linux installation image.
2. Copy all script files (\*.sh) (as well as `mkinitcpio-nbd-*-any.pkg.tar.zst`if `NBD` is used) to the computer.
3. Change directory to where the scripts reside.
4. Edit `install_settings.sh`.
5. Run `install.sh`.

## License

This project is released under 2-Clause BSD License.
