#!/bin/bash

############################################################
#                                                          #
#  Limitations                                             #
#                                                          #
############################################################

# 1. Supports only UEFI-x64 boot mode.
# 2. Supports only GRUB2 bootloader for local boot, GRUB2 or PXELINUX for net boot.
# 3. Simple disk partition scheme with LVM (c.f. install_settings.sh).
# 4. Network manager supports only NetworkManager or systemd-networkd.
# 5. Network configuration supports only wired adapters.

############################################################
#                                                          #
#  Preparation for installation                            #
#                                                          #
############################################################

# Option 1: Remote login with ssh
# ===============================

# Get dynamic IP address or set static IP address
# Run one of the commands at the console:
#   # List IP addresses of all interfaces
#   ip address [show]
#   # List the IP address of an interface
#   ip address show dev <interface>
#   # Add an IP address to an interface
#   ip address add <address>/<prefix_len> [broadcast +] dev <interface>

# Set root password
# Run 'passwd' at the console.

# Copy all install scripts to the computer with scp
# For PuTTY, run one of the commands:
#   pscp -l root <local_path>\install*.sh <address>:/root
#   pscp <local_path>\install*.sh root@<address>:/root

# Log in with ssh using root

# Change directory to where this script resides

# Edit install_settings.sh and run this script

# Option 2: local login at the console
# ====================================

# (Optional) Set large font for boot messages
# Tips: 32-pixel font is the largest
# Edit the kernel parameters:
#   fbcon=font:TER16x32

# (Optional) Set large font for system console
# Run one of the commands at the console:
#   setfont latarcyrheb-sun32
#   setfont ter-132b

# Get dynamic IP address or set static IP address
# Run one of the commands at the console:
#   # List IP addresses of all interfaces
#   ip address [show]
#   # List the IP address of an interface
#   ip address show dev <interface>
#   # Add an IP address to an interface
#   ip address add <address>/<prefix_len> [broadcast +] dev <interface>

# Copy this script to the computer with removable storage device

# Change directory to where this script resides

# Edit install_settings.sh and run this script

############################################################
#                                                          #
#  Installation procedure                                  #
#                                                          #
############################################################

#### Settings & Helper Functions ####
source install_settings.sh
source install_common.sh

#### Installation Steps ####

# 0. Install required packages
#    req_pkgs="pkg1 pkg2 pkg3 ..."
function step_0
{
    local req_pkgs="arch-install-scripts reflector ${fsutil_pkg}"
    local inst_pkgs=
    for pkg_name in ${req_pkgs}; do
      if [ $(pacman -Qs "${pkg_name}" 2>/dev/null | wc -c) -eq 0 ]; then
        inst_pkgs+=" ${pkg_name}"
      fi
    done
    if [ -n "${inst_pkgs}" ]; then
      run_cmd 'pacman --noconfirm -S ${inst_pkgs}'
    fi
}

# 1. Set the console keyboard layout
function step_1
{
    if [ -n "${keymap}" ]; then 
      run_cmd 'loadkeys ${keymap}'
    fi
    if [ -n "${con_font}" ]; then
      local con_map_opt=
      if [ -n "${con_map}" ]; then
        con_map_opt="-m ${con_map}"
      fi
      local con_unimap_opt=
      if [ -n "${con_unimap}" ]; then
        con_unimap_opt="-u ${con_unimap}"
      fi
      run_cmd 'setfont ${con_font} ${con_map_opt} ${con_unimap_opt}'
    fi
}

# 2. Verify the boot mode
function step_2
{
    if [ ! -d /sys/firmware/efi/efivars ]; then
      error_exit "Not in UEFI mode."
    fi
}

# 3. Connect to the internet
function step_3
{
    run_cmd 'ping -c1 archlinux.org'
}

# 4. Update the system clock
function step_4
{
    try_cmd 'timedatectl status | grep -F "NTP service: active" && timedatectl status | grep -F "System clock synchronized: yes"' \
    'timedatectl set-ntp true'
}

# Undo mounts and swapons
function undo_mnts_swpons
{
    if findmnt --mountpoint /mnt &>/dev/null; then
      run_cmd 'umount -R /mnt'
    fi
    if [ "${boot_type}" = "local" ]; then
      if swapon | cut -d ' ' -f 1 | grep -q "^$(realpath -m ${swap_part_path})$"; then
        run_cmd 'swapoff ${swap_part_path}'
      fi
    fi
}

# Erase disks
function erase_disks
{
    # Detect disk partitions
    local disk_path
    local -a part_count
    local all_disks_empty=y
    for ((disk_index=0; disk_index<disks_num; ++disk_index)); do
      disk_path=${disks_dev_paths[${disk_index}]}
      part_count[${disk_index}]=$(run_cmd 'partx -s -g -o NR ${disk_path} | wc -l')
      if [ ${part_count[${disk_index}]} -gt 0 ]; then
        all_disks_empty=n
      fi
    done
    if [ "${all_disks_empty}" = "y" ]; then
      return 0
    fi
    user_action_prompt "Partitions on disks:"
    for ((disk_index=0; disk_index<disks_num; ++disk_index)); do
      disk_path=${disks_dev_paths[${disk_index}]}
      echo [${disk_path}]
      run_cmd 'sfdisk -lq ${disk_path}'
    done
    get_user_key "Delete ALL partitions? (Press 'Y' to delete, others to abort) "
    if [ "${auto_confirm}" != "true" -o "${auto_confirm_partdel}" != "true" ]; then
      if [ "${REPLY}" != "y" -a "${REPLY}" != "Y" ]; then
        error_exit "Disks not empty."
      fi
    fi
    # Remove all VG/LVs, wipe all filesystems and delete all partitions (including PVs)
    local vg_name lv_name part_path pv_name_regex
    pv_name_regex=${disks_dev_paths[0]}${disks_part_prefixes[0]}[0-9]+
    for ((disk_index=1; disk_index<disks_num; ++disk_index)); do
      pv_name_regex+="|${disks_dev_paths[${disk_index}]}${disks_part_prefixes[${disk_index}]}[0-9]+"
    done
    for vg_name in $(run_cmd 'vgs --noheadings -o vg_name -S pv_name=~"${pv_name_regex}" | sort -u'); do
      for lv_name in $(run_cmd 'lvs --noheading -o lv_name ${vg_name}'); do
        part_path=/dev/${vg_name}/${lv_name}
        run_cmd 'wipefs --lock -a ${part_path}'
      done
      run_cmd 'vgremove -y ${vg_name}'
    done
    for ((disk_index=0; disk_index<disks_num; ++disk_index)); do
      disk_path=${disks_dev_paths[${disk_index}]}
      if [ ${part_count[${disk_index}]} -gt 0 ]; then
        for part_path in $(run_cmd 'sfdisk -l -q -o Device ${disk_path} | sed -e "1d"'); do
          run_cmd 'wipefs --lock -a ${part_path}'
        done
        run_cmd 'sfdisk --lock --delete ${disk_path}'
      fi
      run_cmd 'wipefs --lock -a ${disk_path}'
    done
}

# 5. Partition the disks and format the partitions
function step_5
{
    undo_mnts_swpons
    if [ "${boot_type}" = "local" -o -z "${nb_image_file_path}" ]; then
      # Empty disks on user confirmation
      erase_disks
    fi
    # Create partition tables
    if [ "${boot_type}" = "local" ]; then
      run_cmd 'sfdisk --lock --label gpt --wipe always ${disks_dev_paths[0]} << END
-  ${esp_size}   U
-  ${boot_size}  L
-  ${disks_pv_sizes[0]}  V
END'
    else  #[boot_type=net]
      if [ -n "${nb_image_file_path}" ]; then
        if [ -e "${nb_image_file_path}" ]; then
            get_user_key "Overwrite existing disk image file? (Press 'Y' to overwrite, others to abort) "
            if [ "${auto_confirm}" != "true" -o "${auto_confirm_partdel}" != "true" ]; then
              if [ "${REPLY}" != "y" -a "${REPLY}" != "Y" ]; then
                  error_exit "Disk image file exists."
              fi
            fi
        fi
        local nb_image_file_dir=$(dirname ${nb_image_file_path})
        [ -d "${nb_image_file_dir}" ] || run_cmd 'mkdir -p ${nb_image_file_dir}'
        run_cmd 'truncate -s ${nb_image_file_size} ${nb_image_file_path}'
        root_part_path=$(run_cmd 'losetup --find --nooverlap --show ${nb_image_file_path}')
        run_cmd 'wipefs --lock -a ${root_part_path}'
      else
        run_cmd 'sfdisk --lock --label gpt --wipe always ${disks_dev_paths[0]} <<< "-  ${disks_pv_sizes[0]}  V"'
      fi
    fi
    if [ "${boot_type}" = "local" -o -z "${nb_image_file_path}" ]; then
      for ((disk_index=1; disk_index<disks_num; ++disk_index)); do
        run_cmd 'sfdisk --lock --label gpt --wipe always ${disks_dev_paths[disk_index]} <<< "-  ${disks_pv_sizes[disk_index]} V"'
      done
      # Create PV/VG/LVs
      run_cmd 'pvcreate ${disks_pv_paths[*]}'
      run_cmd 'vgcreate ${arch_vg_name} ${disks_pv_paths[*]}'
      if [ "${boot_type}" = "local" ]; then
        run_cmd 'lvcreate -L ${swap_size} -n swap ${arch_vg_name}'
      fi
      local root_size_opt
      if [ "${root_size}" = "-" ]; then
        root_size_opt="-l 100%FREE"
      else
        root_size_opt="-L ${root_size}"
      fi
      run_cmd 'lvcreate ${root_size_opt} -n root ${arch_vg_name}'
      # Format the partitions
      if [ "${boot_type}" = "local" ]; then
        run_cmd 'mkfs.fat -F 32 ${esp_part_path}'
        run_cmd 'mkfs.ext4 ${boot_part_path}'
        run_cmd 'mkswap ${swap_part_path}'
      fi
    fi
    run_cmd 'mkfs -t ${root_fstype} ${root_part_path}'
}

# 6. Mount the file systems
function step_6
{
    undo_mnts_swpons
    run_cmd 'mount ${root_part_path} /mnt'
    if [ "${boot_type}" = "local" ]; then
      if [ ! -d /mnt/boot ]; then
        run_cmd 'mkdir /mnt/boot'
      fi
      run_cmd 'mount ${boot_part_path} /mnt/boot'
      if [ ! -d /mnt/boot/efi ]; then
        run_cmd 'mkdir /mnt/boot/efi'
      fi
      run_cmd 'mount ${esp_part_path} /mnt/boot/efi'
      run_cmd 'swapon ${swap_part_path}'
    fi
}

# 7. Select the mirrors
function step_7
{
    local mirror_list_file=/etc/pacman.d/mirrorlist
    local reflector_conf_file=/etc/xdg/reflector/reflector.conf
    echo "reflector @${reflector_conf_file}"
    cat ${reflector_conf_file}
    try_cmd "?[[ -e ${mirror_list_file} ]] && grep -q '^# When:[[:blank:]]*$(date -uI).*UTC$' ${mirror_list_file}" \
    "reflector @${reflector_conf_file}"
}

# 8. Install essential packages
function step_8
{
    local basic_packages="base linux ${fsutil_pkg}"
    [ "${target_mach_class}" = "physical" ] && basic_packages+=" linux-firmware"
    [ "${boot_type}" = "local" -o "${nb_pxe_prog}" = "grub" ] && basic_packages+=" grub"
    if [ "${boot_type}" = "local" ]; then
      extra_packages="lvm2 efibootmgr ${network_mgr}"
    else  #[boot_type=net]
      extra_packages="mkinitcpio-nfs-utils"
      if [ "${nb_rootfs_type}" = "nfs" ]; then
        extra_packages+=" nfs-utils"
      else
        extra_packages+=" nbd"
      fi
    fi
    run_cmd pacman --noconfirm -Sy archlinux-keyring    # Update keys in case newer packages need newer keys than ones on installation iso.
    run_cmd pacstrap /mnt ${basic_packages} ${extra_packages} ${add_packages}
    if [ "${boot_type}" = "net" -a "${nb_rootfs_type}" = "nbd" ]; then
      run_cmd pacman --noconfirm --root /mnt --dbpath /mnt/var/lib/pacman -U mkinitcpio-nbd-*-any.pkg.tar.zst
    fi
}

# 9. Fstab
function step_9
{
    local fstab_path=/mnt/etc/fstab
    echo "Fstab file:"
    if [ "${boot_type}" = "local" ]; then
      run_cmd 'genfstab -U /mnt | tee ${fstab_path}'
    else
      run_cmd 'tee ${fstab_path} <<< "tmpfs  /var/log  tmpfs  nodev,nosuid,noexec  0  0"'
      [ "${nb_zram_swap_size}" != "0" ] && run_cmd 'tee -a ${fstab_path} <<< "/dev/zram0  none  swap  defaults  0  0"'
    fi
}

# 10. Copy install scripts to the /root of chroot environment
function step_10
{
    local wdir=/mnt/root/autoinstall
    run_cmd 'mkdir -p ${wdir}'
    run_cmd 'cp install_chroot.sh ${wdir}'
    run_cmd 'chmod a+x ${wdir}/install_chroot.sh'
    run_cmd 'cp install_common.sh ${wdir}'
    run_cmd 'cp install_settings.sh ${wdir}'
}

# 11. Chroot and run install_chroot.sh
function step_11
{
    run_cmd 'arch-chroot /mnt /bin/bash -c "cd /root/autoinstall && ./install_chroot.sh"'
}

# 12. Set DNS servers
#     NOTE: resolv.conf can not be edited in arch-chroot environment since it is bind mounted.
function step_12
{
    if [ "${boot_type}" = "local" ]; then
      if [ ${network_mgr} = "systemd" ]; then
        run_cmd ln -sf /run/systemd/resolve/resolv.conf /mnt/etc/resolv.conf
      fi
    else
      local net_dev_index dns1 dns2
      local dns_items=
      local net_dev_count=${#net_devices[*]}
      for ((net_dev_index=0; net_dev_index<net_dev_count; ++net_dev_index)); do
        if [ "${net_ipv4_type[${net_dev_index}]}" = "static" ]; then
          dns1=${net_ipv4_dns1[${net_dev_index}]}
          dns2=${net_ipv4_dns2[${net_dev_index}]}
          if [ -n "${dns1}" ]; then
            dns_items+="nameserver ${dns1}\n"
            if [ -n "${dns2}" ]; then
              dns_items+="nameserver ${dns2}\n"
            fi
          fi
        fi
        if [ "${net_ipv6_type[${net_dev_index}]}" = "static" ]; then
          dns1=${net_ipv6_dns1[${net_dev_index}]}
          dns2=${net_ipv6_dns2[${net_dev_index}]}
          if [ -n "${dns1}" ]; then
            dns_items+="nameserver ${dns1}\n"
            if [ -n "${dns2}" ]; then
              dns_items+="nameserver ${dns2}\n"
            fi
          fi
        fi
      done
      echo "/mnt/etc/resolv.conf:"
      run_cmd 'echo -e "${dns_items}" | tee /mnt/etc/resolv.conf'
    fi
}

# 13. Finish
#       Copy grub netboot images/cfg and unmount /mnt.
#       Detach loop device if image file are used for net boot.
function step_13
{
    if [ "${boot_type}" = "net" ]; then
      [ -d ${nb_tftp_root} ] || run_cmd mkdir -p ${nb_tftp_root}
      run_cmd 'cp -r /mnt/boot/${nb_pxe_prog} ${nb_tftp_root}'
      # NOTE: ${nb_boot_image_dir} must NOT be within single quotes,
      #       since it may refer to another variable.
      eval boot_image_path=${nb_tftp_root}/${nb_boot_image_dir}
      [ -d ${boot_image_path} ] || run_cmd mkdir -p ${boot_image_path}
      run_cmd 'cp /mnt/boot/{vmlinuz-linux,initramfs-linux.img} ${boot_image_path}'
      run_cmd 'chmod -R a+r ${nb_tftp_root}'
    fi
    run_cmd 'umount -R /mnt'
    if [ "${boot_type}" = "net" -a -n "${nb_image_file_path}" ]; then
      run_cmd 'losetup --detach ${root_part_path}'
    fi
    echo -e "${color_green}.OK.${color_reset}"
}

#### Start ####

# Check whether run as root
if [ $EUID -ne 0 ]; then
  echo "Please run this script as root."
  exit
fi

# Set variables used by multiple steps
fsutil_pkg=$(get_fsutil_pkg ${root_fstype} || error_exit "Invalid setting (root_fstype=${root_fstype})")
esp_part_path=${disks_dev_paths[0]}${disks_part_prefixes[0]}1
boot_part_path=${disks_dev_paths[0]}${disks_part_prefixes[0]}2
arch_vg_name=${host_name:-arch}
swap_part_path=/dev/${arch_vg_name}/swap
root_part_path=/dev/${arch_vg_name}/root
if [ "${boot_type}" = "net" -a -n "${nb_image_file_path}" ]; then
  if [ -f ${nb_image_file_path} ]; then
    root_part_path=$(run_cmd 'losetup --find --nooverlap --show ${nb_image_file_path}')
  fi
fi
disks_num=${#disks_dev_paths[*]}
if [ "${boot_type}" = "local" ]; then
  disks_pv_paths[0]=${disks_dev_paths[0]}${disks_part_prefixes[0]}3
else
  disks_pv_paths[0]=${disks_dev_paths[0]}${disks_part_prefixes[0]}1
fi
for ((disk_index=1; disk_index<disks_num; ++disk_index)); do
  disks_pv_paths[${disk_index}]=${disks_dev_paths[${disk_index}]}${disks_part_prefixes[${disk_index}]}1
done

# Run from step_$1 (or first argument if $1 is null)
# to step_$2 (or second argument if $2 is null)
run_steps 0 13 "$1" "$2"
