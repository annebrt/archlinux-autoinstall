#!/bin/bash

#### Settings ####

# Automatically confirm questions except for partiton deletion
# Valid values are: true, false
auto_confirm=true

# Automatically confirm partiton deletion
# Valid values are: true, false
# It is effective only when auto_confirm=true
# !!! USE WITH CAUTION !!!
# There's a chance of data loss!
auto_confirm_partdel=false

# The system to be installed will boot from
# local disks or a boot server (i.e. diskless)
# Valid values are: local, net
boot_type=local

# Installation target machine class
# Valid values are: physical, virtual
target_mach_class=virtual

# Install rootfs on the image file
# Set to null if not on image file (i.e. on disks)
nb_image_file_path=

# Size of the rootfs image file
nb_image_file_size=4G

# Network storage type of rootfs
# Used only when boot from a boot server
# Valid values are: nfs, nbd
nb_rootfs_type=nbd

# Net storage object id of rootfs
# Used only when boot from a boot server
# File system path for NFS, or export name for NBD
nb_rootfs_id=arch

# Boot server IP address
# Used only when boot from a boot server
nb_server_ip=192.168.0.1

# Root directory of TFTP server
nb_tftp_root=/var/lib/tftpboot

# Image directory relative to TFTP root directory
# Boot images are stored in ${nb_tftp_root}/${nb_boot_image_dir}
# NOTE:
#   1) If it refers to another variable, like '${host_name}',
#      the single quotes ('') are required.
#   2) If TFTP root directory is desired, set it to null (blank or "").
nb_boot_image_dir='${host_name}'

# PXE Net Boot Program
# Valid values are: grub, pxelinux
nb_pxe_prog=grub

# PXELINUX configuration file name
# Valid file name could be:
# (1) Client UUID in lowercase hex, e.g.
#         nb_pxelinux_cfg_name=b8945908-d6a6-41a9-611d-74a6ab80b83d
# (2) Hardware type (0x01) and MAC address in lowercase hex, e.g.
#         nb_pxelinux_cfg_name=01-11-22-33-aa-bb-cc
# (3) First few (from all to one) characters of IPv4 address in uppercase hex, e.g.
#         nb_pxelinux_cfg_name=C0A80001
#         nb_pxelinux_cfg_name=C0A8000
#         nb_pxelinux_cfg_name=C0A800
#         nb_pxelinux_cfg_name=C0A80
#         ...
#         nb_pxelinux_cfg_name=C
# (4) Lowercase 'default'.
#         nb_pxelinux_cfg_name=default
nb_pxelinux_cfg_name=default

# GRUB configuration file name suffix
# The file name will be grub.cfg${nb_grub_cfg_suffix}.
# Valid suffix could be:
# (1) 0x01 & MAC address in small hex letters, e.g.
#         nb_grub_cfg_suffix=-01-11-22-33-aa-bb-cc
# (2) First few (from all to one) characters of IPv4 address in uppercase hex, e.g.
#         nb_grub_cfg_suffix=-C0A80001
#         nb_grub_cfg_suffix=-C0A8000
#         nb_grub_cfg_suffix=-C0A800
#         nb_grub_cfg_suffix=-C0A80
#         ...
#         nb_grub_cfg_suffix=-C
# (3) Null string, i.e.
#         nb_grub_cfg_suffix=
nb_grub_cfg_suffix=

# Size of zram swap for netboot installation
# Set to 0 if do not use zram swap
nb_zram_swap_size=128M

# Period and times for waiting operaton to complete
wait_period=5s
wait_count=3

# Keymap
# To list available keymaps, run the command:
#   localectl list-keymaps
# Set to null if not set keymap
keymap=

# Console font
# Available console fonts are in /usr/share/kbd/consolefonts
# Use font name without suffix ('.gz', '.psf.gz', or '.psfu.gz')
# Set to null if not set console font
con_font=

# Console map
# Available console maps are in /usr/share/kbd/consoletrans
# Use map name without '_to_uni.trans'
# Set to null if not set console map
con_map=

# Unicode font map
# Available Unicode font maps are in /usr/share/kbd/unimaps
# Use map name without '.uni'
# Set to null if not set Unicode font map
con_unimap=

# Device paths of disks
disks_dev_paths=(/dev/sda /dev/sdb /dev/nvme0n1)

# Partition prefixes of disks
# Inserted between device path and partiton number
# e.g. 'p' for NVMe disk partitions (nvmeXnYpZ)
disks_part_prefixes=("" "" "p")

# Partition sizes
# Partiton scheme (local boot):
#   |      Disk 0       | Other Disks |
#   -----------------------------------
#   |     |      |      ARCH (VG)     |
#   | ESP | BOOT |--------------------|
#   |     |      |   SWAP   |  ROOT   |
# Partiton scheme (net boot):
#   |  Disk 0  |  Disk 1  |  ...  |  Disk (n-1)  |
#   ----------------------------------------------
#   |                 ARCH (VG)                  |
#   |--------------------------------------------|
#   |                   ROOT                     |
# LVM is used for swap and root partitions.
# Specify partition sizes in MiB ('M'), GiB ('G'), TiB ('T'), etc.
# PV size '-' means all remaining disk space.
# LV root size '-' means all remaining VG space.
disks_pv_sizes=(- - -)
esp_size=100M
boot_size=512M
swap_size=1G
root_size=-

# Filesystem type of rootfs
# Valid values are: ext3, ext4, btrfs, xfs
root_fstype=ext4

# Reflector parameters
# Edit /etc/xdg/reflector/reflector.conf
# If parameters have changed, run the command to update mirror list:
#   reflector @/etc/xdg/reflector/reflector.conf

# Additional packages to install
# Alternatively, these packages can be installed
# after chroot or after system installation.
add_packages="nano man-db man-pages texinfo openssh"

# Time zone
# To list available zones, run the command:
#   timedatectl list-timezones
time_zone=America/New_York

# Locale
# Available locales are listed in /etc/locale.gen
# Use strings in the first column
# 'locales' is a list of locales to be enabled
# Locales are separated by '|', without spaces
# 'lang' is the default locale
locales="en_US.UTF-8|fr_FR.UTF-8"
lang=en_US.UTF-8

# Host name
host_name=myarch

# Network manager
# Valid values are: networkmanager, systemd, "" (null)
# Set to null if do not use any network manager
# No network manager will be installed for netboot
network_mgr=networkmanager

# Network configuration parameters
# Valid values for 'type' are: static, dynamic, disabled.
# If type is dynamic or disabled, other fields are ignored.
# If dns1 is null (i.e. set to ""), dns2 is ignored.
# For netboot, only static dns settings are effective.
     net_devices=(ens1      ens2           )
   net_ipv4_type=(dynamic   static         )
net_ipv4_address=(""        192.168.0.31/24)
net_ipv4_gateway=(""        192.168.0.1    )
   net_ipv4_dns1=(""        1.1.1.1        )
   net_ipv4_dns2=(""        1.0.0.1        )
   net_ipv6_type=(disabled  disabled       )
net_ipv6_address=(""        ""             )
net_ipv6_gateway=(""        ""             )
   net_ipv6_dns1=(""        ""             )
   net_ipv6_dns2=(""        ""             )

# Custom GRUB menu entries
# Set to null to keep custom.cfg intact
custom_grub_menu='
menuentry "Reboot" {
    reboot
}'

# Root password
# IMPORTANT: set a strong password for root!
root_password='root'

# Run some post-installation commands
# These commands will run in chroot environment, which is restrictive
# (e.g. can not start systemd service).
# Return non-zero will cause install_chroot.sh to print an error message
# and exit with non-zero status.
# If no post-installation is desired, delete or comment out all statements
# except for the 'return' statement.
function post_install
{
  run_cmd 'useradd -m arch && echo -e "arch\narch\n" | passwd arch'
  run_cmd 'systemctl enable sshd'
  return 0
}
