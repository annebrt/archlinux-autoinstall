#!/bin/bash

# IMPORTANT: This script must run in chroot environment

#### Settings & Helper Functions ####
source install_settings.sh
source install_common.sh

#### Installation Steps ####

# 51. Time zone
function step_51
{
    run_cmd 'ln -sf /usr/share/zoneinfo/${time_zone} /etc/localtime'
    run_cmd hwclock --systohc
}

# 52. Localization
function step_52
{
    local locale_gen_file=/etc/locale.gen
    # Save default file on first run
    # Restore from default file on following runs
    if [ ! -e ${locale_gen_file}.default ]; then
      run_cmd 'cp ${locale_gen_file} ${locale_gen_file}.default'
    fi
    local regex_lcs=${locales//./\\.}
    run_cmd 'sed -E "s/^#((${regex_lcs})[[:blank:]])/\1/" ${locale_gen_file}.default > ${locale_gen_file}'
    run_cmd locale-gen
    run_cmd 'echo LANG=${lang} > /etc/locale.conf'
    # Make the console keymap/font/map changes persistent
    local vcon_conf_file=/etc/vconsole.conf
    run_cmd 'truncate -s 0 ${vcon_conf_file}'
    if [ -n "${keymap}" ]; then
      run_cmd 'echo KEYMAP=${keymap} >> ${vcon_conf_file}'
    fi
    if [ -n "${con_font}" ]; then
      run_cmd 'echo FONT=${con_font} >> ${vcon_conf_file}'
      if [ -n "${con_map}" ]; then
        run_cmd 'echo FONT_MAP=${con_map} >> ${vcon_conf_file}'
      fi
      if [ -n "${con_unimap}" ]; then
        run_cmd 'echo FONT_UNIMAP=${con_unimap} >> ${vcon_conf_file}'
      fi
    fi
}

# 53. Network configuration
function step_53
{
    # Set hostname
    run_cmd 'echo ${host_name} > /etc/hostname'
    if [ "${boot_type}" = "local" ]; then
      if [ -z "${network_mgr}" ]; then
        return
      fi
      # Enable network manager service
      case ${network_mgr} in
      networkmanager)
        run_cmd systemctl enable NetworkManager
        ;;
      systemd)
        run_cmd systemctl enable systemd-networkd
        run_cmd systemctl enable systemd-resolved
        ;;
      *)
        error_exit "Invalid setting (network_mgr=${network_mgr})"
        ;;
      esac
      # Create the directory where the configuration files reside
      local if_cfg_dir if_cfg_file
      case ${network_mgr} in
      networkmanager)
        if_cfg_dir=/etc/NetworkManager/system-connections
        ;;
      systemd)
        if_cfg_dir=/etc/systemd/network
        ;;
      esac
      if [ ! -d ${if_cfg_dir} ]; then
        run_cmd 'mkdir -p ${if_cfg_dir}'
      fi
      # Create configuration files for each network device
      local net_dev_count=${#net_devices[*]}
      local net_dev_index if_name
      local ip_addr gateway dns1 dns2
      local dns_item dns1_item dns2_item gateway_item
      case ${network_mgr} in
      networkmanager)
        for((net_dev_index=0; net_dev_index<net_dev_count; ++net_dev_index)); do
          if_name=${net_devices[${net_dev_index}]}
          if_cfg_file=${if_cfg_dir}/${if_name}.nmconnection
          echo -e "\nConfiguration for ${if_name}:\n"
          run_cmd 'tee ${if_cfg_file} << END
[connection]
id=conn_${if_name}
uuid=$(uuidgen)
type=ethernet
interface-name=${if_name}
autoconnect=true

END'
          case "${net_ipv4_type[${net_dev_index}]}" in
          "disabled")
            run_cmd 'tee -a ${if_cfg_file} << END
[ipv4]
method=disabled

END'
            ;;
          "static")
            # Static IP address
            ip_addr=${net_ipv4_address[${net_dev_index}]}
            dns1=${net_ipv4_dns1[${net_dev_index}]}
            dns2=${net_ipv4_dns2[${net_dev_index}]}
            gateway=${net_ipv4_gateway[${net_dev_index}]}
            dns_item=
            if [ -n "${dns1}" ]; then
              dns_item="dns=${dns1};"
              if [ -n "${dns2}" ]; then
                dns_item+="${dns2};"
              fi
            fi
            gateway_item=
            if [ -n "${gateway}" ]; then 
              gateway_item="gateway=${gateway}"
            fi
            run_cmd 'tee -a ${if_cfg_file} << END
[ipv4]
method=manual
address1=${ip_addr}
${gateway_item}
${dns_item}

END'
            ;;
          *)
            # Dynamic IP address
            run_cmd 'tee -a ${if_cfg_file} << END
[ipv4]
method=auto

END'
            ;;
          esac
          case "${net_ipv6_type[${net_dev_index}]}" in
          "disabled")
            run_cmd 'tee -a ${if_cfg_file} << END
[ipv6]
method=disabled

END'
            ;;
          "static")
            # Static IP address
            ip_addr=${net_ipv6_address[${net_dev_index}]}
            dns1=${net_ipv6_dns1[${net_dev_index}]}
            dns2=${net_ipv6_dns2[${net_dev_index}]}
            gateway=${net_ipv6_gateway[${net_dev_index}]}
            dns_item=
            if [ -n "${dns1}" ]; then
              dns_item="dns=${dns1};"
              if [ -n "${dns2}" ]; then
                dns_item+="${dns2};"
              fi
            fi
            gateway_item=
            if [ -n "${gateway}" ]; then 
              gateway_item="gateway=${gateway}"
            fi
            run_cmd 'tee -a ${if_cfg_file} << END
[ipv6]
method=manual
address1=${ip_addr}
${gateway_item}
${dns_item}

END'
            ;;
          *)
            # Dynamic IP address
            run_cmd 'tee -a ${if_cfg_file} << END
[ipv6]
method=auto

END'
            ;;
          esac
          run_cmd 'chown root:root ${if_cfg_file}'
          run_cmd 'chmod 0600 ${if_cfg_file}'
        done
        ;;
      systemd)
        local ipv4_type ipv6_type dhcp
        local link_local_addr ipv6_accept_ra
        for((net_dev_index=0; net_dev_index<net_dev_count; ++net_dev_index)); do
          if_name=${net_devices[${net_dev_index}]}
          if_cfg_file=/etc/systemd/network/20-${if_name}.network
          echo -e "\nConfiguration for ${if_name}:\n"
          run_cmd 'tee ${if_cfg_file} << END
[Match]
Name=${if_name}

[Link]
ActivationPolicy=up

END'
          ipv4_type=${net_ipv4_type[${net_dev_index}]}
          ipv6_type=${net_ipv6_type[${net_dev_index}]}
          link_local_addr=no
          ipv6_accept_ra=no
          if [ "${ipv6_type}" = "dynamic" ]; then
            link_local_addr=ipv6
            ipv6_accept_ra=yes
            if [ "${ipv4_type}" = "dynamic" ]; then
              dhcp=yes
            else
              dhcp=ipv6
            fi     
          else
            if [ "${ipv4_type}" = "dynamic" ]; then
              dhcp=ipv4
            else
              dhcp=no
            fi               
          fi
          run_cmd 'tee -a ${if_cfg_file} << END
[Network]
LinkLocalAddressing=${link_local_addr}
IPv6AcceptRA=${ipv6_accept_ra}
DHCP=${dhcp}

END'
          if [ "${net_ipv4_type[${net_dev_index}]}" = "static" ]; then
            ip_addr=${net_ipv4_address[${net_dev_index}]}
            dns1=${net_ipv4_dns1[${net_dev_index}]}
            dns2=${net_ipv4_dns2[${net_dev_index}]}
            gateway=${net_ipv4_gateway[${net_dev_index}]}
            dns1_item=
            dns2_item=
            if [ -n "${dns1}" ]; then
              dns1_item="DNS=${dns1}"
              if [ -n "${dns2}" ]; then
                dns2_item="DNS=${dns2}"
              fi
            fi
            gateway_item=
            if [ -n "${gateway}" ]; then
              gateway_item="Gateway=${gateway}"
            fi
            run_cmd 'tee -a ${if_cfg_file} << END
Address=${ip_addr}
${gateway_item}
${dns1_item}
${dns2_item}

END'
          fi
          if [ "${net_ipv6_type[${net_dev_index}]}" = "static" ]; then
            ip_addr=${net_ipv6_address[${net_dev_index}]}
            dns1=${net_ipv6_dns1[${net_dev_index}]}
            dns2=${net_ipv6_dns2[${net_dev_index}]}
            gateway=${net_ipv6_gateway[${net_dev_index}]}
            dns1_item=
            dns2_item=
            if [ -n "${dns1}" ]; then
              dns1_item="DNS=${dns1}"
              if [ -n "${dns2}" ]; then
                dns2_item="DNS=${dns2}"
              fi
            fi
            gateway_item=
            if [ -n "${gateway}" ]; then
              gateway_item="Gateway=${gateway}"
            fi
            run_cmd 'tee -a ${if_cfg_file} << END
Address=${ip_addr}
${gateway_item}
${dns1_item}
${dns2_item}

END'
          fi
        done
        ;;
      esac
    fi
}

# 54. Initramfs
function step_54
{
    local mk_conf_file=/etc/mkinitcpio.conf
    # Save default conf on first run
    if [ ! -e ${mk_conf_file}.default ]; then
      run_cmd 'cp ${mk_conf_file} ${mk_conf_file}.default'
    fi
    if [ "${boot_type}" = "local" ]; then
      run_cmd 'sed -E "s/^(HOOKS=\(.*block)[[:blank:]]+(filesystems.*\))/\1 lvm2 \2/" ${mk_conf_file}.default > ${mk_conf_file}'
    else
      if [ "${nb_rootfs_type}" = "nfs" ]; then
        run_cmd 'sed s/nfsmount/mount.nfs4/ /usr/lib/initcpio/hooks/net > /usr/lib/initcpio/hooks/netnfs4'
        run_cmd 'cp /usr/lib/initcpio/install/net{,nfs4}'
        run_cmd 'sed -E "s/^(MODULES=\(.*)\)/\1 nfsv4 )/; s/^(HOOKS=\(.*)\)/\1 netnfs4 )/; s#^(BINARIES=\(.*)\)#\1 /usr/bin/mount.nfs4 )#" ${mk_conf_file}.default > ${mk_conf_file}'
      else
        run_cmd 'sed -E "s/^(HOOKS=\(.*)\)/\1 net nbd )/" ${mk_conf_file}.default > ${mk_conf_file}'
      fi
    fi
    run_cmd mkinitcpio -p linux
}

# 55. Root password
function step_55
{
    run_cmd 'echo -e "${root_password}\n${root_password}\n" | passwd'
}

# 56. Boot loader
function step_56
{
    if [ "${boot_type}" = "local" ]; then
      run_cmd grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=archlinux
      run_cmd grub-mkconfig -o /boot/grub/grub.cfg
      if [ -n "${custom_grub_menu}" ]; then
        run_cmd 'echo "${custom_grub_menu}" > /boot/grub/custom.cfg'
      fi
    else  #[${boot_type}=net]
      local zswap_param=
      # Disable zswap if zram swap is enabled
      [ "${nb_zram_swap_size}" != "0" ] && zswap_param="zswap.enabled=0"
      local kernel_cmdline="rw quiet add_efi_memmap ip=dhcp ${zswap_param}"
      if [ "${nb_rootfs_type}" = "nfs" ]; then
        kernel_cmdline+=" nfsroot=${nb_server_ip}:${nb_rootfs_id}"
      else
        kernel_cmdline+=" nbd_host=${nb_server_ip} nbd_name=${nb_rootfs_id} root=/dev/nbd0"
      fi
      # NOTE: ${nb_boot_image_dir} must NOT be within single quotes,
      #       since it may refer to another variable.
      if [ "${nb_pxe_prog}" = "grub" ]; then
        run_cmd grub-mknetdir --net-directory=/boot --subdir=grub
        echo "grub.cfg${nb_grub_cfg_suffix}:"
        run_cmd "tee /boot/grub/grub.cfg${nb_grub_cfg_suffix} <<END
set default=0
set timeout=0
set timeout_style=hidden
insmod efi_gop
menuentry \"Arch Linux\" {
    linux /${nb_boot_image_dir}/vmlinuz-linux ${kernel_cmdline}
    initrd /${nb_boot_image_dir}/initramfs-linux.img
}
END"
      else  #[${nb_pxe_prog}=pxelinux]
        echo "${nb_pxelinux_cfg_name}:"
        run_cmd "tee /boot/pxelinux/pxelinux.cfg/${nb_pxelinux_cfg_name} <<END
prompt 0
default arch-x86_64
label arch-x86_64
kernel ::/${nb_boot_image_dir}/vmlinuz-linux
initrd ::/${nb_boot_image_dir}/initramfs-linux.img
append ${kernel_cmdline}
END"
      fi
    fi
}

# 57. Configure zram device which will be used as swap
function step_57
{
    if [ "${boot_type}" = "net" ]; then
      run_cmd rm -f /etc/modules-load.d/zram.conf /etc/modprobe.d/zram.conf /etc/udev/rules.d/99-zram.rules
      if [ "${nb_zram_swap_size}" != "0" ]; then
        run_cmd 'echo zram > /etc/modules-load.d/zram.conf'
        run_cmd 'echo "options zram num_devices=1" > /etc/modprobe.d/zram.conf'
        run_cmd 'echo '\''KERNEL=="zram0", ATTR{disksize}="'\''${nb_zram_swap_size}'\''" RUN="/usr/bin/mkswap /dev/zram0", TAG+="systemd"'\'' > /etc/udev/rules.d/99-zram.rules'
      fi
    fi
}

# 58. The last step in chroot: run post_install and remove install scripts.
function step_58
{
    run_cmd post_install
    run_cmd 'cd / && rm -rf /root/autoinstall'
}

#### Start ####
# Run from step_$1 (or first argument if $1 is null)
# to step_$2 (or second argument if $2 is null)
run_steps 51 58 "$1" "$2"
