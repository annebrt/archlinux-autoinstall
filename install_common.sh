#!/bin/bash

#### Constants ####
readonly color_reset="\e[0m"
readonly color_black="\e[0;30m"
readonly color_red="\e[0;31m"
readonly color_green="\e[0;32m"
readonly color_yellow="\e[0;33m"
readonly color_blue="\e[0;34m"
readonly color_magenta="\e[0;35m"
readonly color_cyan="\e[0;36m"
readonly color_white="\e[0;37m"

#### Helper Functions ####

# Print error message (combining all arguments) then exit with status ${step_id}
# Error message is in red
function error_exit
{
  echo -e "${color_red}ERROR @${step_id} : $@${color_reset}"
  exit ${step_id}
}

# Print a message $1 to ask for user action
# Message is in cyan
function user_action_prompt
{
  echo -e "${color_cyan}$1${color_reset}"
}

# Print a prompt $1 and get user input key
# Return is NOT required
# REPLY will be cleared first
# Key char is stored in REPLY
function get_user_key
{
  REPLY=
  echo -ne "${color_cyan}$1${color_reset}"
  if [ "${auto_confirm}" = "true" ]; then
    run_cmd 'echo | read -n 1'
  else
    run_cmd 'read -n 1'
  fi  
  echo
}

# Print a prompt $2 and store user input string into $1
# Return is required
# NOT support auto-confirmation
function get_user_str
{
  echo -ne "${color_cyan}$2${color_reset}"
  run_cmd 'read $1'
}

# Run commands ("$1 $2 $3 ...") and test exit status,
# print error message and exit if the command list failed.
# The whole command list or only the parts which would otherwise be
# expanded/substituted by the shell must be single-quoted.
# Tips:
# (1) Put each argument which would otherwise be expanded/substituted
#     by the shell between single quotes.
# (2) Put each argument which already contains quotes (", ' and \)
#     between single quotes.
# (3) Put control characters (>, <, |, |&, ;, &&, ||, etc) between single quotes.
# (4) If the argument contains single quotes, escape each single quote and put
#     other parts of the argument between single quotes.
# Examples:
#   echo *    -->  run_cmd 'echo *'
#              OR  run_cmd echo '*'
#   echo $x   -->  run_cmd 'echo $x'
#              OR  run_cmd echo '$x'
#   echo '*'  -->  run_cmd 'echo '\''*'\'
#              OR  run_cmd echo \''*'\'
#   echo "*"  -->  run_cmd 'echo "*"'
#              OR  run_cmd echo '"*"'
#   echo "'\$x'=$x"    -->  run_cmd 'echo "'\''\$x'\''=$x"'
#                       OR  run_cmd echo '"'\''\$x'\''=$x"'
#   echo a >star       -->  run_cmd 'echo a >star'
#                       OR  run_cmd echo a '>star'
#   echo a >>"s file"  -->  run_cmd 'echo a >>"s file"'
#                       OR  run_cmd echo a '>>"s file"'
#   pstr="v = "; echo -e "a1\ta2\ta3\nbx\tby\tbz\n" | gawk '/[[:digit:]]/ { print "'"$pstr"'" $2 }'
#   -->  run_cmd 'pstr="v = "; echo -e "a1\ta2\ta3\nbx\tby\tbz\n" | gawk '\''/[[:digit:]]/ { print "'\''"$pstr"'\''" $2 }'\'
#    OR  run_cmd 'pstr="v = "'\; echo -e '"a1\ta2\ta3\nbx\tby\tbz\n"' \| gawk \''/[[:digit:]]/ { print "'\''"$pstr"'\''" $2 }'\'
#   tee <<END
#   hello world
#   END
#   -->
#   run_cmd $'tee <<END\nhello world\nEND'
#   OR
#   run_cmd 'tee <<END
#   hello world
#   END'
#   tee <<< "hello world"  -->  run_cmd 'tee <<< "hello world"'
function run_cmd
{
    eval "$@" || error_exit "$@"
}

# Run commands ("$2 $3 $4 ...") at intervals of ${wait_period}
# at most ${wait_count} times until the condition $1 is true.
# If $1 starts with '?', test the condition before first run of commands.
# (In this case the condition is $1 excluding the leading '?'.)
function try_cmd
{
    local cond="${1#\?}"
    if [[ "$1" == \?* ]]; then
      eval "${cond}" && return 0
    fi
    run_cmd "${@:2}"
    eval "${cond}" && return 0
    for ((i=1; i<${wait_count}; ++i)); do
      sleep ${wait_period}
      run_cmd "${@:2}"
      eval "${cond}" && return 0
    done
    error_exit "${cond} AFTER: ${@:2}"
}

# Run from step_$3 to step_$4
# If $3 is null, then start=$1
# If $4 is null, then end=$2
# Check whether (start >= $1) and (start <= end) and (end <= $2)
function run_steps
{
    local step_id=${3:-$1}
    local step_end=${4:-$2}
    if grep -q '[^[:digit:]]' <<< ${step_id}${step_end} \
    || [ ${step_id} -lt $1 -o ${step_id} -gt ${step_end} -o ${step_end} -gt $2 ]; then
      step_id=-1
      error_exit "Invalid start step ($3) and/or end step ($4)."
    fi
    while [ ${step_id} -le ${step_end} ]; do
      step_${step_id}
      let ++step_id
    done
}

# Print utility package name for filesystem $1
function get_fsutil_pkg
{
    local pkgname=
    local exit_status=0
    case "$1" in
    ext3|ext4)
      pkgname="e2fsprogs"
      ;;
    btrfs)
      pkgname="btrfs-progs"
      ;;
    xfs)
      pkgname="xfsprogs"
      ;;
    *)
      exit_status=1
      ;;
    esac
    echo $pkgname
    return ${exit_status}
}
